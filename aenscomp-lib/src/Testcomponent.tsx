import React from "react";

export interface TestProps{
    yourName: string;
}

export function Testcomponent({yourName} : TestProps) {
    return (
        <div>
            <h2>Hello, {yourName}!</h2>
            <p>This is a new test component!!</p>
        </div>
    )
}